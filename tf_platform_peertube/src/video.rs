/*
 * Copyright 2021 Julian Schmidhuber <github@schmiddi.anonaddy.com>
 *
 * This file is part of Tubefeeder-extractor.
 *
 * Tubefeeder-extractor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tubefeeder-extractor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tubefeeder-extractor.  If not, see <https://www.gnu.org/licenses/>.
 */

use async_trait::async_trait;
use chrono::Duration;
use serde_json::Map;

use crate::PTSubscription;
use tf_core::{ExtraVideoInfo, Subscription, Video, DATE_FORMAT};
use tf_utils::rss::{FromItemAndSub, Item};

#[derive(Clone)]
pub struct PTVideo {
    pub(crate) url: String,
    pub(crate) title: String,
    pub(crate) uploaded: chrono::NaiveDateTime,
    pub(crate) subscription: PTSubscription,
    pub(crate) thumbnail_url: String,
    pub(crate) duration: Option<Duration>,
}

impl std::hash::Hash for PTVideo {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.url.hash(state);
        self.title.hash(state);
        self.subscription.hash(state);
    }
}

impl std::cmp::PartialEq for PTVideo {
    fn eq(&self, other: &Self) -> bool {
        self.url == other.url
            && self.title == other.title
            && self.subscription == other.subscription
    }
}

impl std::cmp::Eq for PTVideo {}

impl PTVideo {
    pub fn new<T: AsRef<str>>(
        url: T,
        title: T,
        uploaded: chrono::NaiveDateTime,
        subscription: PTSubscription,
        thumbnail_url: T,
        duration: Option<Duration>,
    ) -> Self {
        Self {
            url: url.as_ref().to_owned(),
            title: title.as_ref().to_owned(),
            uploaded,
            subscription,
            thumbnail_url: thumbnail_url.as_ref().to_owned(),
            duration,
        }
    }
}

impl std::convert::TryFrom<Vec<String>> for PTVideo {
    type Error = ();

    fn try_from(strings: Vec<String>) -> Result<Self, Self::Error> {
        let url_opt = strings.get(0);
        let title = strings.get(1);
        let uploaded = strings.get(2);
        let sub_name = strings.get(3);
        let sub_id = strings.get(4);
        let sub_base_url = strings.get(5);
        let thumbnail_url = strings.get(6);
        let duration = strings.get(7);
        match (
            url_opt,
            title,
            uploaded,
            sub_name,
            sub_id,
            sub_base_url,
            thumbnail_url,
            duration,
        ) {
            (
                Some(url),
                Some(tit),
                Some(upl),
                Some(sub_n),
                Some(sub_i),
                Some(sub_u),
                Some(thu),
                dur,
            ) => {
                let upl_date = chrono::NaiveDateTime::parse_from_str(upl, DATE_FORMAT);
                if let Ok(upl) = upl_date {
                    let sub = PTSubscription::new_with_name(sub_u, sub_i, sub_n);
                    Ok(PTVideo::new(
                        url,
                        tit,
                        upl,
                        sub,
                        thu,
                        dur.and_then(|d| d.parse::<i64>().ok())
                            .map(|d| Duration::seconds(d)),
                    ))
                } else {
                    Err(())
                }
            }
            _ => Err(()),
        }
    }
}

impl From<PTVideo> for Vec<String> {
    fn from(video: PTVideo) -> Self {
        let mut result = vec![];
        result.push(video.url());
        result.push(video.title());
        result.push(video.uploaded().format(DATE_FORMAT).to_string());
        let sub = video.subscription();
        result.push(sub.name().unwrap_or_else(|| "".to_string()));
        result.push(sub.id());
        result.push(sub.base_url());
        result.push(video.thumbnail_url());
        result.push(
            video
                .duration()
                .map(|d| d.num_seconds().to_string())
                .unwrap_or_default(),
        );
        result
    }
}

#[derive(thiserror::Error, Debug)]
pub enum PTFetchError {
    #[error("failed to fetch the information")]
    Request(#[from] reqwest::Error),
    #[error("the returned information was not in the expected json format")]
    Json(#[from] serde_json::Error),
}

#[async_trait]
impl tf_core::Video for PTVideo {
    type Subscription = PTSubscription;
    type FetchError = PTFetchError;

    fn url(&self) -> String {
        self.url.clone()
    }

    fn title(&self) -> String {
        self.title.clone()
    }

    fn uploaded(&self) -> chrono::NaiveDateTime {
        self.uploaded.clone()
    }

    fn subscription(&self) -> Self::Subscription {
        self.subscription.clone()
    }

    fn thumbnail_url(&self) -> String {
        self.thumbnail_url.clone()
    }

    fn duration(&self) -> Option<Duration> {
        self.duration.clone()
    }

    async fn extra_information_with_client(
        &self,
        client: &reqwest::Client,
    ) -> Result<Option<tf_core::ExtraVideoInfo>, Self::FetchError> {
        let url = self.url();
        let Some(video_id) = url.split("/").last() else {
            return Ok(None);
        };
        let body = client
            .get(self.subscription.base_url() + "/api/v1/videos/" + video_id)
            .send()
            .await?
            .text_with_charset("utf-8")
            .await?;
        let json: Map<String, serde_json::Value> = serde_json::from_str(&body)?;
        let Some(serde_json::Value::String(desc)) = json.get("description") else {
            return Ok(None);
        };
        let Some(serde_json::Value::Number(likes)) = json.get("likes") else {
            return Ok(None);
        };
        let Some(serde_json::Value::Number(dislikes)) = json.get("dislikes") else {
            return Ok(None);
        };
        let Some(serde_json::Value::Number(views)) = json.get("views") else {
            return Ok(None);
        };
        Ok(Some(ExtraVideoInfo {
            description: Some(desc.clone()),
            likes: likes.as_u64(),
            dislikes: dislikes.as_u64(),
            views: views.as_u64(),
        }))
    }
}

impl FromItemAndSub<PTSubscription> for PTVideo {
    fn from_item_and_sub(i: Item, sub: PTSubscription) -> Self {
        Self {
            title: i.media_title,
            url: i.link,
            uploaded: i.pub_date,
            subscription: sub,
            thumbnail_url: i
                .media_thumbnail
                .into_iter()
                .next()
                .map(|m| m.url)
                .unwrap_or_default(),
            // XXX: Can be different durations.
            duration: i
                .media_group
                .content
                .into_iter()
                .map(|d| d.duration)
                .map(Duration::seconds)
                .next(),
        }
    }
}
