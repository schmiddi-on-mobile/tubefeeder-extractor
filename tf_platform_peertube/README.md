# Tubefeeder Extractor

This is a part of [Tubefeeder Extractor](https://gitlab.com/schmiddi-on-mobile/tubefeeder-extractor).

This contains the implementation of a [peertube](https://joinpeertube.org/) extractor from a rss feed.
