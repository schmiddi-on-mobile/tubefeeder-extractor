# Tubefeeder Extractor

This is a part of [Tubefeeder Extractor](https://gitlab.com/schmiddi-on-mobile/tubefeeder-extractor).

This contains the implementation of a [YouTube](https://www.youtube.com/) extractor from a [Piped instance](https://github.com/TeamPiped/Piped).
