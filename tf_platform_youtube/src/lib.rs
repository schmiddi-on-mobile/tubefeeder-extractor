/*
 * Copyright 2021 Julian Schmidhuber <github@schmiddi.anonaddy.com>
 *
 * This file is part of Tubefeeder-extractor.
 *
 * Tubefeeder-extractor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tubefeeder-extractor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tubefeeder-extractor.  If not, see <https://www.gnu.org/licenses/>.
 */

//! A implementation of a [Tubefeeder-extractor](https://gitlab.com/schmiddi-on-mobile/tubefeeder-extractor) for YouTube using [Piped](https://github.com/TeamPiped/Piped).

pub use piped;

mod pipeline;
mod subscription;
mod video;

use piped::apis::configuration::Configuration;
pub use piped::apis::unauthenticated_api as piped_unauth;
pub use pipeline::YTPipeline;
pub use subscription::YTSubscription;
pub use video::YTVideo;

const PIPED_API_URL: &'static str = "https://pipedapi.kavin.rocks";

pub fn piped_api_url() -> String {
    match std::env::var("PIPED_API_URL") {
        Ok(url) => url.trim_end_matches("/").to_owned(),
        Err(_e) => PIPED_API_URL.to_string(),
    }
}

pub fn configuration(client: Option<reqwest::Client>) -> Configuration {
    let mut c = Configuration::new();
    c.base_path = piped_api_url();

    if let Some(client) = client {
        c.client = client;
    }

    c
}
