/*
 * Copyright 2021 Julian Schmidhuber <github@schmiddi.anonaddy.com>
 *
 * This file is part of Tubefeeder-extractor.
 *
 * Tubefeeder-extractor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tubefeeder-extractor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tubefeeder-extractor.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::piped_unauth;
use crate::subscription::YTSubscription;

use async_trait::async_trait;
use chrono::Duration;

use piped::models::StreamItem;
use tf_core::{ErrorStore, ExtraVideoInfo, Subscription, Video, DATE_FORMAT};

const YOUTUBE_URL: &'static str = "https://www.youtube.com";

#[derive(Clone, Debug)]
pub struct YTVideo {
    pub(crate) url: String,
    pub(crate) title: String,
    pub(crate) uploaded: chrono::NaiveDateTime,
    pub(crate) subscription: YTSubscription,
    pub(crate) thumbnail_url: String,
    pub(crate) duration: Option<Duration>,
}

impl std::hash::Hash for YTVideo {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.url.hash(state);
        self.title.hash(state);
        self.subscription.hash(state);
    }
}

impl std::cmp::PartialEq for YTVideo {
    fn eq(&self, other: &Self) -> bool {
        self.url == other.url
            && self.title == other.title
            && self.subscription == other.subscription
    }
}

impl std::cmp::Eq for YTVideo {}

impl YTVideo {
    pub fn new<T: AsRef<str>>(
        url: T,
        title: T,
        uploaded: chrono::NaiveDateTime,
        subscription: YTSubscription,
        thumbnail_url: T,
        duration: Option<Duration>,
    ) -> Self {
        Self {
            url: url.as_ref().to_owned(),
            title: title.as_ref().to_owned(),
            uploaded,
            subscription,
            thumbnail_url: thumbnail_url.as_ref().to_owned(),
            duration,
        }
    }
}

impl std::convert::TryFrom<Vec<String>> for YTVideo {
    type Error = ();

    fn try_from(strings: Vec<String>) -> Result<Self, Self::Error> {
        let url_opt = strings.get(0);
        let title = strings.get(1);
        let uploaded = strings.get(2);
        let sub_name = strings.get(3);
        let sub_id = strings.get(4);
        let thumbnail_url = strings.get(5);
        let duration = strings.get(6);
        match (
            url_opt,
            title,
            uploaded,
            sub_name,
            sub_id,
            thumbnail_url,
            duration,
        ) {
            (Some(url), Some(tit), Some(upl), Some(sub_n), Some(sub_i), Some(thu), dur) => {
                let upl_date = chrono::NaiveDateTime::parse_from_str(upl, DATE_FORMAT);
                if let Ok(upl) = upl_date {
                    let sub = YTSubscription::new_with_name(sub_i, sub_n);
                    Ok(YTVideo::new(
                        url,
                        tit,
                        upl,
                        sub,
                        thu,
                        dur.and_then(|d| d.parse::<i64>().ok())
                            .map(|d| Duration::seconds(d)),
                    ))
                } else {
                    Err(())
                }
            }
            _ => Err(()),
        }
    }
}

impl From<YTVideo> for Vec<String> {
    fn from(video: YTVideo) -> Self {
        let mut result = vec![];
        result.push(video.url());
        result.push(video.title());
        result.push(video.uploaded().format(DATE_FORMAT).to_string());
        let sub = video.subscription();
        result.push(sub.name().unwrap_or_else(|| "".to_string()));
        result.push(sub.id());
        result.push(video.thumbnail_url());
        result.push(
            video
                .duration()
                .map(|d| d.num_seconds().to_string())
                .unwrap_or_default(),
        );

        result
    }
}

#[async_trait]
impl tf_core::Video for YTVideo {
    type Subscription = YTSubscription;
    type FetchError = tf_core::Error;

    fn url(&self) -> String {
        self.url.clone()
    }

    fn title(&self) -> String {
        self.title.clone()
    }

    fn subscription(&self) -> Self::Subscription {
        self.subscription.clone()
    }

    fn thumbnail_url(&self) -> String {
        self.thumbnail_url.clone()
    }

    fn uploaded(&self) -> chrono::NaiveDateTime {
        self.uploaded
    }

    fn duration(&self) -> Option<Duration> {
        self.duration.clone()
    }

    async fn extra_information_with_client(
        &self,
        client: &reqwest::Client,
    ) -> Result<Option<tf_core::ExtraVideoInfo>, Self::FetchError> {
        let c = crate::configuration(Some(client.to_owned()));
        if let Some(last) = self
            .url()
            .strip_prefix(&(YOUTUBE_URL.to_owned() + "/watch?v="))
        {
            let info = piped_unauth::stream_info(&c, last)
                .await
                .map_err(|e| crate::subscription::piped_to_tubefeeder_error(&e))?;
            Ok(Some(ExtraVideoInfo {
                description: info.description,
                likes: info.likes.and_then(|l| Some(l as u64)),
                // Even though Piped provides this information, it is not accurate.
                dislikes: None,
                views: info.views.and_then(|l| Some(l as u64)),
            }))
        } else {
            log::warn!(
                "Cannot extract video ID from video with url: {}",
                self.url()
            );
            Ok(None)
        }
    }

    async fn thumbnail_with_client(&self, client: &reqwest::Client) -> image::DynamicImage {
        let thumbnail_url = self.thumbnail_url();
        log::debug!("Getting thumbnail from url {}", thumbnail_url);
        let response = client.get(&thumbnail_url).send().await;

        if response.is_err() {
            log::error!(
                "Failed getting thumbnail for url {}, use default",
                thumbnail_url
            );
            return self.default_thumbnail();
        }

        let parsed = response.unwrap().bytes().await;

        if parsed.is_err() {
            log::error!(
                "Failed getting thumbnail for url {}, use default",
                thumbnail_url
            );
            return self.default_thumbnail();
        }

        let parsed_bytes = parsed.unwrap();

        if let Some(image) = <Self as Video>::convert_image(&parsed_bytes) {
            image
        } else {
            self.default_thumbnail()
        }
    }

    fn convert_image(data: &[u8]) -> Option<image::DynamicImage> {
        image::load_from_memory(&data).ok()
    }

    fn default_thumbnail(&self) -> image::DynamicImage {
        image::DynamicImage::new_rgba8(1, 1)
    }

    async fn thumbnail(&self) -> image::DynamicImage {
        self.thumbnail_with_client(&reqwest::Client::new()).await
    }
}

impl YTVideo {
    pub(crate) fn from_related_stream(
        _errors: &ErrorStore,
        v: &StreamItem,
        subscription: YTSubscription,
    ) -> Self {
        YTVideo {
            url: format!("{}{}", YOUTUBE_URL, v.url),
            title: v.title.clone(),
            subscription,
            uploaded: chrono::NaiveDateTime::from_timestamp_opt(
                v.uploaded.expect("YTVideo should have a uploaded date") / 1000,
                0,
            )
            .unwrap_or_default(),
            thumbnail_url: v.thumbnail.clone(),
            duration: Some(Duration::seconds(v.duration.into())),
        }
    }
}
