/*
 * Copyright 2021 Julian Schmidhuber <github@schmiddi.anonaddy.com>
 *
 * This file is part of Tubefeeder-extractor.
 *
 * Tubefeeder-extractor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tubefeeder-extractor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tubefeeder-extractor.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::video::YTVideo;
use std::collections::HashMap;
use std::sync::{Arc, Mutex};

use crate::piped_unauth;
use async_trait::async_trait;
use piped::models::{SearchFilter, SearchItem};
use tf_core::{ErrorStore, GeneratorWithClient, Subscription, SubscriptionList};

/// A [`YTSubscription`] to a YouTube-Channel. The Youtube-Channel is referenced by the channel id.
#[derive(Debug, Clone, Eq, Hash)]
pub struct YTSubscription {
    /// The channel id.
    id: String,
    name: Option<String>,
}

impl std::cmp::PartialEq for YTSubscription {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl YTSubscription {
    /// Create a new [`YTSubscription`] using the given channel id.
    pub fn new(id: &str) -> Self {
        YTSubscription {
            id: id.to_owned(),
            name: None,
        }
    }

    /// Create a new [`YTSubscription`] using the given channel id and name.
    pub fn new_with_name(id: &str, name: &str) -> Self {
        YTSubscription {
            id: id.to_owned(),
            name: Some(name.to_owned()),
        }
    }

    /// Try to get a subscription using youtube search.
    /// This will try to search the given query in youtube filtered only to channels
    /// and return the first result if it exists.
    pub async fn try_from_search<S: AsRef<str>>(query: S) -> Option<Self> {
        log::debug!("Getting channel from query {}", query.as_ref());

        let c = crate::configuration(None);

        let result = piped_unauth::search(&c, query.as_ref(), SearchFilter::Channels).await;
        if let Ok(channel_search) = result {
            if let Some(items) = channel_search.items {
                log::debug!("Got back a result with {} items", items.len());
                items.get(0).map(|i| i.into())
            } else {
                log::error!("No result");
                None
            }
        } else {
            log::error!("Got back a error: {}", result.err().unwrap());
            None
        }
    }

    /// Get the channel id of the [`YTSubscription`].
    pub fn id(&self) -> String {
        self.id.clone()
    }

    /// Try to get the channel name from the channel id.
    pub async fn update_name(&self, client: &reqwest::Client) -> Option<String> {
        let c = crate::configuration(Some(client.to_owned()));

        if let Ok(channel) = piped_unauth::channel_info_id(&c, &self.id).await {
            channel.name
        } else {
            None
        }
    }

    fn with_name(&self, name: &str) -> Self {
        Self {
            id: self.id.clone(),
            name: Some(name.to_string()),
        }
    }
}

impl Subscription for YTSubscription {
    type Video = YTVideo;

    /// Get the name of the [`YTSubscription`].
    fn name(&self) -> Option<String> {
        self.name.clone()
    }
}

impl std::fmt::Display for YTSubscription {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.name().unwrap_or_else(|| self.id()))
    }
}

impl std::convert::TryFrom<Vec<String>> for YTSubscription {
    type Error = ();

    fn try_from(strings: Vec<String>) -> Result<Self, Self::Error> {
        if let Some(value) = strings.get(0) {
            Ok(YTSubscription::new(value))
        } else {
            Err(())
        }
    }
}

impl From<YTSubscription> for Vec<String> {
    fn from(sub: YTSubscription) -> Self {
        vec![sub.id]
    }
}

#[derive(Clone)]
pub struct YTSubscriptionList(pub Arc<Mutex<SubscriptionList<YTSubscription>>>);

#[async_trait]
impl GeneratorWithClient for YTSubscriptionList {
    type Item = YTVideo;
    type Iterator = std::vec::IntoIter<Self::Item>;

    async fn generate_with_client(
        &self,
        errors: &ErrorStore,
        client: &reqwest::Client,
    ) -> Self::Iterator {
        let subs = self
            .0
            .lock()
            .expect("Poisoned mutex: YT Subscription List")
            .subscriptions();
        let num_subs = subs.len();

        // For few subs, generate the videos without bulk, as bulk often has none or few videos in
        // the case that the subscriptions did not upload for a longer while.
        if num_subs <= 10 {
            let mut videos = vec![];
            for s in subs {
                videos.append(&mut s.generate_with_client(errors, client).await.collect());
            }
            return videos.into_iter();
        }

        log::debug!(
            "Generating YT videos from channels {:?}",
            subs.iter().map(|s| s.name().unwrap_or_else(|| s.id()))
        );

        let c = crate::configuration(Some(client.to_owned()));

        let videos_res =
            piped_unauth::feed_unauthenticated(&c, subs.iter().map(|s| s.id()).collect()).await;

        if let Err(e) = &videos_res {
            log::error!(
                "Error generating youtube videos from subscriptions {:?}: {}",
                subs,
                e
            );
            for _ in 0..num_subs {
                errors.add(piped_to_tubefeeder_error(e));
            }
            return vec![].into_iter();
        }

        let videos = videos_res.unwrap();

        let map_id_to_subscription: HashMap<String, YTSubscription> =
            subs.iter().map(|s| (s.id(), s.clone())).collect();

        Box::new(videos.into_iter().map(|v| {
            let uploader_url = v.uploader_url.as_ref();
            let uploader_name = v.uploader_name.as_ref();
            YTVideo::from_related_stream(
                errors,
                &v,
                map_id_to_subscription
                    .get(
                        uploader_url
                            .expect("YTVideo should have an uploader_url")
                            .strip_prefix("/channel/")
                            .unwrap_or_default(),
                    )
                    .expect("YTVideo got unknown channel uploader id")
                    .with_name(&uploader_name.expect("YTVideo should have an uploader_name")),
            )
        }))
        .collect::<Vec<YTVideo>>()
        .into_iter()
    }
}

#[async_trait]
impl GeneratorWithClient for YTSubscription {
    type Item = YTVideo;
    type Iterator = std::vec::IntoIter<Self::Item>;

    async fn generate_with_client(
        &self,
        errors: &ErrorStore,
        client: &reqwest::Client,
    ) -> Self::Iterator {
        log::debug!(
            "Generating YT videos from channel {}",
            self.name().unwrap_or_else(|| self.id())
        );

        let c = crate::configuration(Some(client.to_owned()));
        let channel_res = piped_unauth::channel_info_id(&c, &self.id).await;

        if let Err(e) = &channel_res {
            log::error!(
                "Error generating youtube videos from subscription {:?}: {}",
                self,
                e
            );
            errors.add(piped_to_tubefeeder_error(e));
            return vec![].into_iter();
        }

        if let Err(e) = &channel_res {
            log::error!(
                "Error generating youtube videos from subscription {:?}: {}",
                self,
                e
            );
            errors.add(piped_to_tubefeeder_error(e));
            return vec![].into_iter();
        }

        let channel = channel_res.unwrap();
        let videos = channel.related_streams.unwrap_or_default();
        let name = channel.name.unwrap_or_default();

        Box::new(
            videos
                .into_iter()
                .map(|v| YTVideo::from_related_stream(errors, &v, self.with_name(name.as_ref()))),
        )
        .collect::<Vec<YTVideo>>()
        .into_iter()
    }
}

impl From<&SearchItem> for YTSubscription {
    fn from(item: &SearchItem) -> Self {
        let SearchItem::Channel(boxed) = item else {
            // piped allows only channels to be filtered before the search. This Trait should not be used in other situatation
            panic!("Cannot convert {:?} into YTSubscription", item)
        };
        let c = boxed.as_ref();
        Self {
            id: c
                .url
                .as_ref()
                .expect("ChannelItem should have url")
                .split('/')
                .last()
                .unwrap_or_default()
                .to_string(),
            name: c.name.clone(),
        }
    }
}

pub fn piped_to_tubefeeder_error<T>(error: &piped::apis::Error<T>) -> tf_core::Error {
    match error {
        piped::apis::Error::Reqwest(e) => tf_core::NetworkError(e.to_string()).into(),
        piped::apis::Error::Serde(e) => tf_core::ParseError(e.to_string()).into(),
        piped::apis::Error::Io(e) => tf_core::ParseError(e.to_string()).into(),
        piped::apis::Error::ResponseError(e) => {
            tf_core::NetworkError(format!("a response. Status code {}", e.status)).into()
        }
    }
}
