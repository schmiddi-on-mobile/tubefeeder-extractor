# Tubefeeder-extractor

__Archived__: This repository is not maintained anymore.
Use [pipeline-backend](https://gitlab.com/schmiddi-on-mobile/pipeline-backend) instead.

This is the backend for [Pipeline](https://gitlab.com/schmiddi-on-mobile/Pipeline)
This could of course also be used for other projects.

## Supported Features

Currently subscribing, filters, playlists and "currently playing" are supported.

## Supported Platforms

Currently YouTube, PeerTube and LBRY are supported.

## Writing for a new platform

If you would like to support this by writing a new backend, please take a look at [the wiki](https://gitlab.com/schmiddi-on-mobile/pipeline/-/wikis/Add-support-for-a-new-platform) and do not hesitate contact us in the [Pipeline Matrix Room](https://matrix.to/#/%23tubefeeder:matrix.org).

## Documentation

Most of the source code is now has basic documentatation.
The documentation can be generated with `cargo doc --open``.
