//! Utility functions for [Tubefeeder-extractor](https://gitlab.com/schmiddi-on-mobile/tubefeeder-extractor), e.g. parsing of RSS and
//! parsing of human-readable times.

pub mod rss;
