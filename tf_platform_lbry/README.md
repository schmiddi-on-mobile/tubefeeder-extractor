# Tubefeeder Extractor

__Unmaintained__: Due to [Lbry shutting down](https://twitter.com/LBRYcom/status/1678866789407551489), this will not be maintained anymore.

This is a part of [Tubefeeder Extractor](https://github.com/schmiddi-on-mobile/tubefeeder-extractor).

This contains the implementation of a [lbry](https://lbry.com/) extractor from a rss feed.
