# Tubefeeder Extractor

This is a part of [Tubefeeder Extractor](https://gitlab.com/schmiddi-on-mobile/tubefeeder-extractor).

This compines multiple piplines as defined in the [core](https://crates.io/crates/tf_core) with included [filtering](https://crates.io/crates/tf_filter).
